#
# Define the varialbles within your Terraform configuration 
# update/replace variables to match your deployment  
# Provider - OpenStack
#
variable "openstack_user_name" {
  description = "The username for the Tenant."
  default  = "demo"
}

variable "openstack_tenant_name" {
  description = "The name of the Tenant."
  default  = "demo"
}

variable "openstack_password" {
  description = "The password for the Tenant."
  default  = <removed>
}

variable "openstack_auth_url" {
  description = "The endpoint url to connect to OpenStack."
  default  = "https://os.corp.local:5000/v3"
}

variable "openstack_keypair" {
  description = "The keypair to be used."
  default  = "test-pair"
}

variable "insecure" {
  type = "string"
  description = "Accept self signed SSL certificates?"
  default = "true"
}

variable "flavor" {
  description = "flavor for vm boot"
  default = "2"
}

variable "name_server" {
  description = "name servers to be used for VMs"
  default = ["192.168.110.10"]
}

variable "pub-pool" {
  description = "name of the floating ip pool"
  default = "ext-net"
}

variable "image-name" {
  description = "name of the image to boot"
  default = "ubuntu-16.04-server-cloudimg-amd64"
}

variable "az-zone" {
  description = "name of AZ zone to boot"
  default = "nova"
}

variable "external-gw" {
  description = "Openstack external Network ID"
  default = "8338dbf8-67de-41b2-8da3-a38c1ecc607e"
}

